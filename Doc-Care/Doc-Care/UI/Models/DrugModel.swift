//
//  PrescriptionModel.swift
//  Doc-Care
//
//  Created by Hüseyin Sönmez on 15.10.2018.
//  Copyright © 2018 Hüseyin Sönmez. All rights reserved.
//

import Foundation

enum DrugPosology: String {
    case Peroral
    case Invasive
    
    func desc() -> String {
        switch self {
        case .Peroral:
            return "Peroral (Ağızdan)"
        case .Invasive:
            return "IV/IM (İğne İle)"
        }
    }
    
    init?(index: Int32) {
        switch index {
        case 0: self = .Peroral
        case 1: self = .Invasive
        default: return nil
        }
    }
    
}

class DrugModel: Equatable {
    
    init(index: Int) {
        self.Index = index
    }
    convenience init() {
        self.init(index: 0)
    }
    var Index: Int! = 0
    var Name: String? = nil
    var Dose: Float! = 0.0
    var Posology: DrugPosology! = .Peroral
    
    static func ==(lhs: DrugModel, rhs: DrugModel) -> Bool {
        return lhs.Name == rhs.Name
    }
}

extension DrugModel: ModelTransformator {
    typealias T = DrugEntity
    
    func transform(entity: DrugEntity) -> Void {
        self.Name = entity.name
        self.Dose = entity.dose
        self.Posology = DrugPosology(index: entity.posology)
    }
    
}


