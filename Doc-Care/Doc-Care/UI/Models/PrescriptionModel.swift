//
//  PrescriptionModel.swift
//  Doc-Care
//
//  Created by Hüseyin Sönmez on 15.10.2018.
//  Copyright © 2018 Hüseyin Sönmez. All rights reserved.
//

import Foundation

class PrescriptionModel: Equatable {

    var SignedOn: Date? = nil
    var Code: String? = nil
    
    static func ==(lhs: PrescriptionModel, rhs: PrescriptionModel) -> Bool {
        return lhs.SignedOn == rhs.SignedOn
    }
}

extension PrescriptionModel: ModelTransformator {
    typealias T = PrescriptionEntity
    
    func transform(entity: PrescriptionEntity) -> Void {
        self.SignedOn = entity.signedOn ?? Date()
        self.Code = entity.code
    }
    
}


