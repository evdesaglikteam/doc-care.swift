//
//  ViewControllerExtensions.swift
//  Doc-Care
//
//  Created by Hüseyin Sönmez on 27.10.2018.
//  Copyright © 2018 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func application() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
}
