//
//  EntityTransformator.swift
//  Doc-Care
//
//  Created by Hüseyin Sönmez on 27.10.2018.
//  Copyright © 2018 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import CoreData

protocol ModelTransformator {
    associatedtype T: NSManagedObject
    func transform(entity: T) -> Void
    
}
