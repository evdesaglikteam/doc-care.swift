//
//  DrugFormController.swift
//  Doc-Care
//
//  Created by Hüseyin Sönmez on 15.10.2018.
//  Copyright © 2018 Hüseyin Sönmez. All rights reserved.
//

import UIKit
import Eureka

class DrugFormController: FormViewController, TypedRowControllerType {

    typealias RowValue = DrugModel
    var row: RowOf<DrugModel>!
    var onDismissCallback: ((UIViewController) -> Void)?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.build()
    }
    
    var model: DrugModel? = nil
    
    init(model: DrugModel) {
        super.init(style: .plain)
        self.model = model
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DrugFormController {
    private func build() {
        form
        +++ Section("İLAÇ")
            <<< TextRow("drug"){
                    $0.value = self.model?.Name
                    $0.title = "İlaç İsmi"
                    $0.placeholder = "Reçete Edilecek İlaç İsmini Girin"
                    $0.add(rule: RuleRequired())
                    $0.validationOptions = .validatesOnChangeAfterBlurred
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        cell.tintColor = .red
                    } else {
                        self.model?.Name = row.value ?? "Hatalı İsim"
                    }
                }
            <<< SliderRow(){
                    $0.value = self.model?.Dose
                    $0.title = "İlaç Dozu"
                    $0.displayValueFor = { value in
                        return "\(String(value ?? 0.0)) mg"
                    }
                }.onChange {
                    self.model?.Dose = $0.value ?? 0.0
                }
            <<< SwitchRow(){
                    $0.value = self.model?.Posology == .Peroral
                    $0.title = "Pozoloji"
                    $0.displayValueFor = { value in
                        return value ?? true ? DrugPosology.Peroral.desc() : DrugPosology.Invasive.desc()
                    }
                }.onChange {
                    self.model?.Posology = $0.value ?? true ? DrugPosology.Peroral : DrugPosology.Invasive
                }
            <<< ButtonRow() {
                    $0.title = "İlaç Ekle"
                }.onCellSelection { cell, row in
                    if self.form.rows.allSatisfy({ $0.validate().count == 0 }) {
                        self.submit()
                    } else {
                        self.failed(message: "Form Alanlarında Hata Var")
                    }
                }
    }
    
    private func submit() {
        self.onDismissCallback?(self)
        self.navigationController?.popViewController(animated: true)
    }
    
    private func failed(message: String) {
        let alert = UIAlertController(title: "Hata", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Anlaşıldı", style: .destructive, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
    }
}

