//
//  ViewController.swift
//  Doc-Care
//
//  Created by Hüseyin Sönmez on 14.10.2018.
//  Copyright © 2018 Hüseyin Sönmez. All rights reserved.
//

import UIKit
import Eureka

class PrescriptionViewController: FormViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.build()
    }
    var entity: PrescriptionEntity? = nil
}

extension PrescriptionViewController {
    func build() {
        form +++ Section("Demografik")
            <<< TextRow("code"){
                $0.title = "A01"
                $0.disabled = true
            }
            <<< TextRow(){
                    $0.title = "İsim Soyisim"
                    $0.placeholder = "Hasta İsmi Soyismi"
                    $0.add(rule: RuleRequired())
                    $0.validationOptions = .validatesOnChangeAfterBlurred
            }.cellUpdate { cell, row in
                let _ = row.validate()
                if !row.isValid {
                    cell.tintColor = .red
                } else if cell.tag != -1 {
                    cell.tag = -1
                    self.form
                        +++ MultivaluedSection(multivaluedOptions: [.Reorder, .Insert, .Delete],
                                               header: "REÇETE EKRANI",
                                               footer: "'Yeni İlaç Ekle' Butonu İlgili Forma Aktarır") {
                                                $0.tag = "PrescriptionPanel"
                                                $0.showInsertIconInAddButton = true
                                                $0.multivaluedRowToInsertAt = { index in
                                                if let code = (self.form.rowBy(tag: "code") as! TextRow).value {
                                                    var templateRow = ButtonRowWithPresent<DrugFormController>() { button in
                                                        button.title = "İlaç Ekle"
                                                        button.presentationMode = .show(controllerProvider: ControllerProvider.callback {
                                                            
                                                            return DrugFormController(model: DrugModel())
                                                            }, onDismiss: { vc in
                                                                if let pvc = vc as? DrugFormController {
                                                                    if let model = pvc.model {
                                                                        button.title = model.Name
                                                                    }
                                                                }
                                                                vc.navigationController?.popViewController(animated: true)
                                                        })
                                                    }
                                                    for drug in self.application().fetchPrescription(by: code)?.drugs as [DrugEntity] {
                                                        templateRow += ButtonRowWithPresent<DrugFormController>() { button in
                                                            button.title = drug.Name
                                                            button.presentationMode = .show(controllerProvider: ControllerProvider.callback {
                                                                
                                                                return DrugFormController(model: DrugModel())
                                                                }, onDismiss: { vc in
                                                                    if let pvc = vc as? DrugFormController {
                                                                        if let model = pvc.model {
                                                                            button.title = model.Name
                                                                        }
                                                                    }
                                                                    vc.navigationController?.popViewController(animated: true)
                                                            })
                                                        }
                                                    }
                                                    return templateRow
                                                } else {
                                                    return ButtonRowWithPresent<DrugFormController>() { button in
                                                        button.title = "İlaç Ekle"
                                                        button.presentationMode = .show(controllerProvider: ControllerProvider.callback {
                                                            
                                                            return DrugFormController(model: DrugModel())
                                                            }, onDismiss: { vc in
                                                                if let pvc = vc as? DrugFormController {
                                                                    if let model = pvc.model {
                                                                       button.title = model.Name
                                                                    }
                                                                }
                                                                vc.navigationController?.popViewController(animated: true)
                                                        })
                                                    }
                                                }
                                            }
                    }
                }
            }
            <<< PhoneRow(){
                $0.title = "Cep Telefonu"
                $0.placeholder = "Hastanın Şifre Telefonu"
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChangeAfterBlurred
                }.cellUpdate { cell, row in
                    let _ = row.validate()
                    if !row.isValid {
                        cell.tintColor = .red
                    }
                }
    }
}
